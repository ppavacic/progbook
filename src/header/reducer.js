import {FETCH_USER_INFO} from "../application/constants.js";

export const userInfo = (state = null, action) => {
  switch(action.type) {
    case FETCH_USER_INFO:
      return action.payload.data;
    default:
      return state;
  }
}
