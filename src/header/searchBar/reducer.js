import {SEARCHBAR_CHANGE} from "../../application/constants";

export const searchBarQuery = (state = null, action) => {
  switch(action.type) {
    case SEARCHBAR_CHANGE:
      return action.payload;
    default:
      return state;
  }
}
