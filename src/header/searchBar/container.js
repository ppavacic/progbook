import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {withRouter} from 'react-router-dom';

import {handleChange} from "./actions";
import {Button, FormGroup, FormControl, Form} from "react-bootstrap";
import {fetchRepos} from "../../search/repoTab/actions";
import {fetchUsers} from "../../search/peopleTab/actions";

const searchFunction = (event, props) => {
  event.preventDefault();
  props.history.push("/search/" + props.searchBarQuery);
	props.fetchRepos(props.searchBarQuery, props.userInfo.gitlab_api_key);
  props.fetchUsers("/search/" + props.searchBarQuery, props.loginInfo);
}


class SearchBar extends React.Component {
  render() {
    return(
        <Form className="searchForm" inline onSubmit={(event) => searchFunction(event, this.props)}>
          <FormGroup className="searchGroup">
            <FormControl type="text" name="searchBar" className="searchBar" placeholder="Search for people, repos..." onChange={(event) => this.props.handleChange(event)}/>
            <Button className="searchButton" type="submit">Search</Button>
          </FormGroup>
        </Form>
    );
  }
}

function mapStateToProps({searchBarQuery, loginInfo, userInfo}) {
	return {
		searchBarQuery,
    loginInfo,
    userInfo
	};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({handleChange, fetchRepos, fetchUsers}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SearchBar));
