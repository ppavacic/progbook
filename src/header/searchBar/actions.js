import {SEARCHBAR_CHANGE} from "../../application/constants";

export const handleChange = (event) => {
  return({
    type: SEARCHBAR_CHANGE,
    payload: event.target.value
  });
}
