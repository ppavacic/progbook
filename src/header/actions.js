import axios from "axios";
import {LOGOUT, FETCH_USER_INFO, URL_DATABASE, buildDatabaseConf} from "../application/constants";
import {deleteState} from "../application/store";


export const logout = (history) => {
	deleteState();
	history.push("/");
	return {
		type: LOGOUT
	}
}
export const fetchUserInfo = (userToken) => {
  let request = axios.get(URL_DATABASE + "/userInfo", buildDatabaseConf(userToken));
  return {
    type: FETCH_USER_INFO,
    payload: request
  };
};
