import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Image, Panel, Button} from "react-bootstrap";
import {withRouter} from "react-router-dom";
import SearchBar from "./searchBar/container";
import {logout, fetchUserInfo} from "./actions";
import {fallBackUserImage, URL_FRONTEND} from "../application/constants";
import "./style.css";

//https://i.imgur.com/fnCdEqv.jpg 64x64
//https://i.imgur.com/N7xkpKY.jpg 40x40

class Header extends React.Component {
  componentDidMount() {
    this.props.fetchUserInfo(this.props.loginInfo);
  }
  render() {
    if(!this.props.userInfo) {
      return <div/>
    }
    return(
      <Panel className="header static">
        <div className="homeButton clickable" onClick={() => this.props.history.push("/")}>
          Ⱂrogbook
        </div>
        <SearchBar />
        <div className="header-alerts">
            <Button onClick={() => this.props.history.push("/conversations")}>
              <span role="img" aria-label="conversations">💬</span>
            </Button>
        </div>
          <Image className="header-userImage entry-image" circle src={this.props.userInfo.usr_img ? URL_FRONTEND + this.props.userInfo.usr_img : fallBackUserImage} onClick={() => this.props.history.push("/user/" + this.props.userInfo.user_id)} />
          <Button className="header-logout" onClick={() => this.props.logout(this.props.history)}>
            Log out
          </Button>
      </Panel>
    );
  }
}

function mapStateToProps({loginInfo, userInfo}) {
  return {loginInfo, userInfo};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    logout,
    fetchUserInfo
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
