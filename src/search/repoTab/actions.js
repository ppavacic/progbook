import axios from "axios";
import {FETCH_REPOS, URL_GITLAB, buildGitlabConfig, GITLAB_PUB_KEY} from "../../application/constants";
const URL_GITLAB_SEARCH = URL_GITLAB + "/search?scope=projects&search=";

export const fetchRepos = (searchQuery, gitlabApiKey) => {
  let request = axios.get(URL_GITLAB_SEARCH + searchQuery, buildGitlabConfig(gitlabApiKey ? gitlabApiKey : GITLAB_PUB_KEY));

  return({
    type: FETCH_REPOS,
    payload: request
  });
}
