import React from "react";
import {connect} from "react-redux";
import RepoEntry from "../../application/entry/repo/container";
import {bindActionCreators} from "redux";

class RepoTab extends React.Component {
  render() {
    if(!this.props.searchRepoList) {
      return (<div className="loading-animation" />);
    }
    if(!this.props.searchRepoList.data) {
      return (<div>No results</div>);
    }
    return(
      <div className="repo-results results">
        {this.props.searchRepoList.data.map(entry => {
          //id, image, name, following, description, link
          return (<RepoEntry key={entry.id} id={entry.id} image={entry.avatar_url} link={entry.web_url}
            name={entry.name} description={entry.description} following={this.props.followedRepoList[entry.id]}/>);
        })}
      </div>
    );
  }
}

function mapStateToProps({searchRepoList, followedRepoList}) {
  return({
    searchRepoList,
    followedRepoList
  });
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(RepoTab);
