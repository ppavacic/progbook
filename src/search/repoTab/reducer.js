import {FETCH_REPOS} from "../../application/constants";

export const searchRepoList = (state = null, action) => {
  switch (action.type) {
    case FETCH_REPOS:
      return action.payload;
    default:
      return state;
  }
}
