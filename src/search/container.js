import React from "react";
import {connect} from "react-redux";
import {Tabs, Tab} from "react-bootstrap";
import RepoTab from "./repoTab/container";
import PeopleTab from "./peopleTab/container";
//{this.props.searchQuery}

class Search extends React.Component {
  render() {
    return(
      <div className="route search">
        <Tabs defaultActiveKey={"people"} id="search-tabs">
          <Tab eventKey={"people"} title="People">
            <PeopleTab />
          </Tab>
          <Tab eventKey={"repos"} title="Repositories">
            <RepoTab />
          </Tab>
        </Tabs>

      </div>

    );
  }
}

function mapStateToProps({searchBarQuery}) {
  return({
    searchBarQuery,
  });
}


export default connect(mapStateToProps)(Search);
