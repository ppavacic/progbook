import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {withRouter} from "react-router";
import _ from "lodash";
import PersonEntry from "../../application/entry/person/container";
import {fetchUsers} from "./actions";

const renderUserList = (jsonData) => {
  return _.map(jsonData, (user) => {
    return(
      <PersonEntry key={user.usr_id} name={user.usr_name} image={user.usr_img}
        quote={user.usr_quote} id={user.usr_id} following={user.following}
      />
    );
  })
}

class PeopleTab extends React.Component {
  componentDidMount() {
    this.props.fetchUsers(this.props.location.pathname, this.props.loginInfo);
  }
  render() {
    return(
      <div className="people-results results">
        {renderUserList(this.props.searchUsersList)}
      </div>
    );
  }
}

function mapStateToProps({searchUsersList, loginInfo}) {
  return {
    searchUsersList,
    loginInfo
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchUsers
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PeopleTab));
