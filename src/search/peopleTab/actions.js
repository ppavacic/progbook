import axios from "axios";
import {URL_DATABASE, FETCH_PEOPLE, buildDatabaseConf} from "../../application/constants";

export const fetchUsers = (databaseUrlExtension, userToken) => {
  let request = axios.get(URL_DATABASE + databaseUrlExtension, buildDatabaseConf(userToken));
  return {
    type: FETCH_PEOPLE,
    payload: request
  }
}
