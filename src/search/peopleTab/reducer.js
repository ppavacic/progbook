import {FETCH_PEOPLE} from "../../application/constants";

export const searchUsersList = (state = null, action) => {
  switch (action.type) {
    case FETCH_PEOPLE:
      return action.payload.data
    default:
      return state;
  }
}
