import axios from "axios";
import {FETCH_USERPAGE_DATA, URL_DATABASE, buildDatabaseConf} from "../application/constants";

export const fetchUserPageData = (userToken, databaseExtensionLink) => {

  let request = axios.get(URL_DATABASE + databaseExtensionLink, buildDatabaseConf(userToken));
  return {
    type: FETCH_USERPAGE_DATA,
    payload: request
  };
}
