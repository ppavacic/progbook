import React from "react";
import {Well, Image, Button} from "react-bootstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {withRouter} from "react-router";
import {fetchUserPageData} from "./actions";
import {fallBackUserImage} from "../application/constants";
import EntryPerson from "../application/entry/person/container";
import EditProfile from "./editProfile/container";
import _ from "lodash";
import "./style.css";

const renderPosts = (posts) => {
  return _.map(posts, (post) => {
    return(<Well className="entry-text" key={post.post_id}>{post.content_txt}</Well> );
  });
}

//without redux
class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showEditProfile: false,
    }
    this.editProfileTrigger = this.editProfileTrigger.bind(this);
  }
  componentDidMount() {
    this.props.fetchUserPageData(this.props.loginInfo, this.props.location.pathname);
  }

  editProfileTrigger () {
    this.setState({
      showEditProfile: !this.state.showEditProfile
    });
  }
  render() {
    if(!this.props.userPageData || !this.props.userInfo) {
      return <div className="loading-animation" />;
    }
    let stalkersProfile = this.props.userInfo.user_id === this.props.userPageData.usr_id;
    return(
      <div className="route user">
        <div className="profile-info">
          <div className="profile-picture-container center" >
            <Image className="profile-picture" src={this.props.userPageData.image ? this.props.userPageData.image : fallBackUserImage} square="true" responsive />
          </div>

          {stalkersProfile ? <Button onClick={this.editProfileTrigger}>Edit profile</Button> : ""}
          {this.state.showEditProfile ? <EditProfile editProfileToggle={this.editProfileTrigger}/> : ""}
          <EntryPerson id={this.props.userPageData.usr_id} image="" name={this.props.userPageData.usr_name}
            following={stalkersProfile ? null : this.props.userPageData.following} quote={this.props.userPageData.usr_quote}/>
        </div>
        {renderPosts(this.props.userPageData.usr_posts)}
      </div>
    );
  }
}

function mapStateToProps({userPageData, loginInfo, userInfo}) {
  return{
    userPageData,
    userInfo,
    loginInfo
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchUserPageData,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(User));
