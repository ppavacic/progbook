import React from "react";
import axios from "axios";
import {Image, Modal, Button, Form, FormGroup, FormControl, Checkbox} from "react-bootstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {withRouter} from "react-router";
import {fallBackUserImage, URL_FRONTEND} from "../../application/constants";
import {URL_GITLAB, buildGitlabConfig} from "../../application/constants";
import {editProfileSubmit} from "./actions";
import {fetchUserPageData} from "../actions";
import "./style.css";
//some parts are done withou react just for learning
class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayName: this.props.userPageData.usr_name,
      APIKey: "",
      quote: null,
      changeAPIKey: false,
      gitlabId: null,
      gitlabName: null
    };
    this.displayNameChanged = this.displayNameChanged.bind(this);
    this.APIKeyChanged = this.APIKeyChanged.bind(this);
    this.quoteChanged = this.quoteChanged.bind(this);
    this.changeAPIKeyChanged = this.changeAPIKeyChanged.bind(this);
    this.saveButtonValidator = this.saveButtonValidator.bind(this);
  }
  render() {
    return(
      <Modal show={true}>
        <Modal.Body className="edit-header">
          Click on image to change it
          <Image className="profile-picture-edit" src={this.props.userPageData.image ? URL_FRONTEND + this.props.userPageData.image : fallBackUserImage} />
          <Form className="profile-info-edit">
    	        <FormGroup className="profile-info-group" controlId="validationCustom01">
                <br />
                Name
    	          <FormControl type="text" value={this.state.displayName} onChange={this.displayNameChanged}/>
                <br />
                <Checkbox onChange={(event) => this.changeAPIKeyChanged(event)}> Change API key </Checkbox>
    	          <FormControl type="password" readOnly={!this.state.changeAPIKey} onChange={(event) => this.APIKeyChanged(event)} value={this.state.APIKey}
                    placeholder={this.state.changeAPIKey ? "Leave empty for public key to be used" : "Key will be unchanged" }/>
                <span style={!this.saveButtonValidator() ? {"display": "none"} : {}}>API key is invalid</span>
                <span style={(this.saveButtonValidator() || !this.state.changeAPIKey || !this.state.APIKey) ? {"display": "none"} :
                  {}}>Identified as: {this.state.gitlabName}</span>
                <br />
                Personal quote
    	          <FormControl componentClass="textarea" placeholder="Change your quote" onChange={(event) => this.quoteChanged(event)}/>
    	        </FormGroup>
    	    </Form>

        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.editProfileToggle}>Close</Button>
          <Button bsStyle="primary" type="submit" disabled={this.saveButtonValidator()}
            onClick={(event) => {
                this.props.editProfileSubmit(event, this.props.loginInfo, this.state);
                this.props.editProfileToggle();
                this.props.fetchUserPageData(this.props.loginInfo, this.props.location.pathname);
              }
            }>Save changes</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  saveButtonValidator = () => {
    return this.state.changeAPIKey && !this.state.gitlabId && this.state.APIKey;
  }

  displayNameChanged = (event) => {
    this.setState({
      displayName: event.target.value
    });
  }
  APIKeyChanged = (event) => {
    this.setState({
      APIKey: event.target.value
    });
    axios.get(URL_GITLAB + "/user", buildGitlabConfig(event.target.value))
    .then((response) => {
      return response;
    })
    .catch((err) => {
      this.setState({gitlabId: null, gitlabName: null})
    })
    .then((response) => this.setState({gitlabId: response.data.id, gitlabName: response.data.name}));
  }
  quoteChanged = (event) => {
    this.setState({
      quote: event.target.value
    });
  }
  changeAPIKeyChanged = (event) => {
    this.setState({
      changeAPIKey: !this.state.changeAPIKey,
      APIKey: ""
    });
  }
}

function mapStateToProps({userPageData, loginInfo, userInfo}) {
  return{
    userPageData,
    loginInfo
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    editProfileSubmit,
    fetchUserPageData
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditProfile));
