import axios from "axios";
import {URL_DATABASE, EDIT_PROFILE, buildDatabaseConfJson} from "../../application/constants";

export const editProfileSubmit = (event, loginInfo, state) => {
  event.preventDefault();
  let json = {
    "name": state.displayName,
    "key": state.APIKey,
    "change_key": state.changeAPIKey,
    "quote": state.quote,
    "gitlab_id": state.gitlabId
  };
  let request = axios.post(URL_DATABASE + "/user/updateInfo", json, buildDatabaseConfJson(loginInfo));
  return {
    "type": EDIT_PROFILE,
    "payload": request
  };
}
