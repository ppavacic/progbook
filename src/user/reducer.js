import {FETCH_USERPAGE_DATA} from "../application/constants";
export const userPageData = (state = null, action) => {
  switch (action.type) {
    case FETCH_USERPAGE_DATA:
      return action.payload.data;
    default:
      return state;
  }
}
