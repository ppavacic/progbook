import {KEY} from "../env/KEY.js";

//const DATABASE_ADDRESS = "http://192.168.0.16";
const DATABASE_ADDRESS = "http://localhost";
const DATABASE_PORT = "8000";

export const URL_DATABASE = DATABASE_ADDRESS + ":" + DATABASE_PORT;
export const URL_FRONTEND = "http://localhost:3000";
export const URL_GITLAB = "https://gitlab.com/api/v4";
export const GITLAB_PUB_KEY = "HprDDeqHxJ1MvbzJqBNd";

export const fallBackImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/480px-No_image_available.svg.png";
export const fallBackRepoImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/2000px-GitLab_Logo.svg.png";
export const fallBackUserImage = URL_FRONTEND + "/img/blank_profile.png";



export const FETCH_USER_INFO = "USER_USER_INFO";
export const SEARCHBAR_CHANGE = "SEARCHBAR_CHANGE";
export const FETCH_PEOPLE = "FETCH_PEOPLE";
export const FETCH_REPOS = "FETCH_REPOS";
export const FETCH_USERPAGE_DATA = "FETCH_USERPAGE_DATA";
export const FETCH_HOMEPAGE_POSTS = "FETCH_HOMEPAGE_POSTS";
export const FETCH_CONVERSATIONS = "FETCH_CONVERSATIONS";
export const FETCH_MY_REPO_LIST = "FETCH_MY_REPO_LIST";
export const FETCH_FAVORITES_REPO_LIST = "FETCH_FAVORITES_REPO_LIST";
export const FETCH_FOLLOWED_REPO_LIST = "FETCH_FOLLOWED_REPO_LIST";
export const FETCH_FOLLOWED_REPO_DATA = "FETCH_FOLLOWED_REPO_DATA";
export const FETCH_FOLLOWED_REPO_ISSUES = "FETCH_FOLLOWED_REPO_ISSUES";
export const FETCH_FOLLOWED_REPO_ISSUES_USERS = "FETCH_FOLLOWED_REPO_ISSUES_USERS";
export const FETCH_POPULAR_REPO_LIST = "FETCH_POPULAR_REPO_LIST";
export const SUBMIT_POST = "SUBMIT_POST";
export const FETCH_MESSAGES = "FETCH_MESSAGES";
export const POST_MESSAGE = "POST_MESSAGE";
export const REGISTER = "REGISTER";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const FETCH_NEWCONVERSATION_DATA = "FETCH_NEWCONVERSATION_DATA";
export const DELETE_NEWCONVERSATION_DATA = "DELETE_NEWCONVERSATION_DATA";
export const FOLLOW_PERSON = "FOLLOW_PERSON";
export const UNFOLLOW_PERSON = "UNFOLLOW_PERSON";
export const FOLLOW_REPO = "FOLLOW_REPO";
export const UNFOLLOW_REPO = "UNFOLLOW_REPO";
export const FAVORITE_REPO = "FAVORITE_REPO";
export const UNFAVORITE_REPO = "UNFAVORITE_REPO";
export const UNFAVORITE_POST = "UNFAVORITE_POST";
export const FAVORITE_POST = "FAVORITE_POST";
export const EDIT_PROFILE = "EDIT_PROFILE";

export const defaultGitlabConfig = {
    headers: {
    "PRIVATE-TOKEN": KEY
  }
};

export const buildDatabaseConf = (key) => {
  return {
    headers: {
      "User-Token": key,
      "Content-Type": "text/plain"
    }
  };
};

export const buildDatabaseConfJson = (key) => {
  return {
    headers: {
      "User-Token": key,
      "Content-Type": "application/json"
    }
  };
};

export const buildGitlabConfig = (gitlabKey) => {
    return ({
        headers: {
          "PRIVATE-TOKEN": gitlabKey
      }
  });
};
