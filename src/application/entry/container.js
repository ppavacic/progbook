import React from "react";
import {Image, Well, Button} from "react-bootstrap";
import {withRouter} from "react-router-dom";
import {fallBackUserImage} from "../constants";
//#FUZZY rewrite whole entries and if possible remove file

const noText = (str) => {
  if(!str) {
    return(<span className="no-text">"Empty"</span>);
  }
  return str;
}

export const entryHeader = (props) => {
  //if we have case with user on our page or external link
  let external = (props.link ? true : false);
  if(external){
    return(
      <a href={props.link} className="entry-header">
        <Image className="entry-image" src={props.image ? props.image : fallBackUserImage} rounded responsive />
        <h4 className="entry-name">{noText(props.name)}</h4>
      </a>
    );
  } else {
    return (
      //need user_id, name, image
      <span className="entry-header" onClick={() => props.history.push("/user/" + props.user_id)}>
        <Image className="entry-image" src={props.image ? props.image : fallBackUserImage} circle responsive />
        <h4 className="entry-name">{noText(props.name)}</h4>
      </span>
    );
  }
}

//header + content(entry-text (or it is possible to put other react component inside))
const bigEntry = (props) => {
  return(
    <div className="entry post">
      {entryHeader(props)}
      <Well className="entry-text">
        {noText(props.content)}
        <Button className="entry-button" onClick={props.entryButtonFunction}>{props.favorite_count} <span role="img" aria-label="favorite">⭐</span></Button>
      </Well>
    </div>
  );
}

const smallEntry = (props) => {
  return (
    <a href={props.link}>
      <div className="entry-entry">
        <Image className="entry-image" src={props.image ? props.image : fallBackUserImage} rounded responsive/>
        <div className="entry-name">
          {props.name}
        </div>
      </div>
    </a>
  );
}


class Entry extends React.Component {
  render() {
    if(!this.props) {
      return(<div className="loading-animation" />);
    }
    if(this.props.size === "small") {
      return smallEntry(this.props);
    }
    return bigEntry(this.props);
  }
}


export default withRouter(Entry);
