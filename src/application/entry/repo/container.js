import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {noText} from "../common";
import {Well, Image, Button} from "react-bootstrap";
import {followRepo, unfollowRepo, favoriteRepo, unfavoriteRepo} from "./actions";
import {fallBackRepoImage} from "../../constants";


//id, image, name, following, description, link
class RepoEntry extends React.Component {
  render() {
    return(
      <div className="entry post">
        <a href={this.props.link} className="entry-header clickable">
          <Image className="entry-image" src={this.props.image ? this.props.image : fallBackRepoImage} rounded responsive />
          <h4 className="entry-name">{noText(this.props.name)}</h4>
        </a>
        <Well className="entry-text">
          {noText(this.props.description)}
          {favoriteButton(this.props)}
          {followButton(this.props)}
        </Well>
      </div>
    );
  }
}

function mapStateToProps({loginInfo}) {
  return {
    loginInfo
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    followRepo,
    unfollowRepo,
    favoriteRepo,
    unfavoriteRepo
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RepoEntry));


const followButton = (props) => {
  if (!props.following) {
    return <Button className="entry-button" onClick={() => props.followRepo(props.loginInfo, props.id)}>Follow</Button>
  } else {
    return <Button className="entry-button" onClick={() => props.unfollowRepo(props.loginInfo, props.id)}>Unfollow</Button>
  }
}

const favoriteButton = (props) => {
  if (!props.favorited) {
    return <Button className="entry-button" onClick={() => props.favoriteRepo(props.loginInfo, props.id)}><span role="img" aria-label="favorite">⭐</span></Button>
  } else {
    return <Button className="entry-button" onClick={() => props.unfavoriteRepo(props.loginInfo, props.id)}><span role="img" aria-label="favorite">⭐</span></Button>
  }
}
