import axios from "axios";
import {URL_DATABASE, buildDatabaseConf, FOLLOW_REPO, UNFOLLOW_REPO, FAVORITE_REPO, UNFAVORITE_REPO} from "../../constants";

export const followRepo = (userToken, repoId) => {
  let request = axios.post(URL_DATABASE + "/repo/follow/" + repoId, null, buildDatabaseConf(userToken));

  return {
    type: FOLLOW_REPO,
    payload: request
  };
}

export const unfollowRepo = (userToken, repoId) => {
  let request = axios.post(URL_DATABASE + "/repo/unfollow/" + repoId, null, buildDatabaseConf(userToken));

  return {
    type: UNFOLLOW_REPO,
    payload: request
  };
}

export const favoriteRepo = (userToken, repoId) => {
  let request = axios.post(URL_DATABASE + "/repo/favorite/" + repoId, null, buildDatabaseConf(userToken));

  return {
    type: FAVORITE_REPO,
    payload: request
  };
}

export const unfavoriteRepo = (userToken, repoId) => {
  let request = axios.post(URL_DATABASE + "/repo/unfavorite/" + repoId, null, buildDatabaseConf(userToken));

  return {
    type: UNFAVORITE_REPO,
    payload: request
  };
}
