import axios from "axios";
import {URL_DATABASE, buildDatabaseConf, FOLLOW_PERSON, UNFOLLOW_PERSON} from "../../constants";

export const followPerson = (userToken, stalkeeId) => {
  let request = axios.post(URL_DATABASE + "/user/follow/" + stalkeeId, null, buildDatabaseConf(userToken));

  return {
    type: FOLLOW_PERSON,
    payload: request
  };
}

export const unfollowPerson = (userToken, stalkeeId) => {
  let request = axios.post(URL_DATABASE + "/user/unfollow/" + stalkeeId, null, buildDatabaseConf(userToken));

  return {
    type: UNFOLLOW_PERSON,
    payload: request
  };
}
