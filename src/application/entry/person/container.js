import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {noText} from "../common";
import {Well, Image, Button} from "react-bootstrap";
import {followPerson, unfollowPerson} from "./actions";
import {fallBackUserImage, URL_FRONTEND} from "../../constants";


//id, image(value = "" for 'dont display'), name, following, quote
class PersonEntry extends React.Component {
  render() {
    return(
      <div className="entry post">
        <span className="entry-header clickable" onClick={() => this.props.history.push("/user/" + this.props.id)}>
          {displayImage(this.props.image)}
          <h4 className="entry-name">{noText(this.props.name)}</h4>
        </span>
        <Well className="entry-text">
          {noText(this.props.quote)}
          {followButton(this.props)}
          <Button className="entry-button" onClick={() => this.props.history.push("/contact/" + this.props.id)}>Private Message</Button>
        </Well>
      </div>
    );
  }
}

function mapStateToProps({loginInfo}) {
  return {
    loginInfo
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    followPerson,
    unfollowPerson
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PersonEntry));


const followButton = (props) => {
  if(props.following === null) return <React.Fragment />;

  if (props.following === false) {
    return <Button className="entry-button" onClick={() => props.followPerson(props.loginInfo, props.id)}>Follow</Button>
  } else {
    return <Button className="entry-button" onClick={() => props.unfollowPerson(props.loginInfo, props.id)}>Unfollow</Button>
  }
}

const displayImage = (image) => {
  if (image === "")  return <React.Fragment />;
  else {
    return (
      <Image className="entry-image" src={image ? URL_FRONTEND + image : fallBackUserImage}  circle responsive />
  );

  }
}
