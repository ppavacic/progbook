	import React from "react";
import Entry from "./container";
import _ from "lodash";

export const jsonDataToEntry = (jsonData) => {
  return _.map(jsonData, (post, key) => {
    return(
      <Entry key={key} name={post.name} image={post.image}
        favorite_count={post.favorite_count} content={post.text}
        entryButtonFunction={null} user_id={post.user_id}
      />
    );
  })
}
	