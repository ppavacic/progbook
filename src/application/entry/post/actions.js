import axios from "axios";
import {URL_DATABASE, buildDatabaseConf, FAVORITE_POST, UNFAVORITE_POST} from "../../constants";

export const favoritePost = (userToken, postId, postKey) => {
  let request = axios.post(URL_DATABASE + "/post/favorite/" + postId, null, buildDatabaseConf(userToken));
  console.log("fav key: ", postKey);
  return {
    type: FAVORITE_POST,
    payload: request,
    postKey
  };
}

export const unfavoritePost = (userToken, postId, postKey) => {
  let request = axios.post(URL_DATABASE + "/post/unfavorite/" + postId, null, buildDatabaseConf(userToken));

  return {
    type: UNFAVORITE_POST,
    payload: request,
    postKey
  };
}
