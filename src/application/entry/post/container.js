import React from "react";
import {bindActionCreators} from "redux";
import {connect } from "react-redux";
import {fallBackUserImage} from "../../constants";
import {Well, Button, Image} from "react-bootstrap";
import {withRouter} from "react-router";
import {noText} from "../common";
import {favoritePost, unfavoritePost} from "./actions";

//usr.(id, img, name), post.(id, content, date, fav_cnt, favorites, favorited)
//passed as usr, post
class PostEntry extends React.Component {
  render() {
    return(
      <div className="entry post">
        <span className="entry-header clickable" onClick={() => this.props.history.push("/user/" + this.props.usr.id)}>
          <Image className="entry-image" src={this.props.usr.img ? this.props.usr.img : fallBackUserImage} circle responsive />
          <h4 className="entry-name">{noText(this.props.usr.name)}</h4>
        </span>
        <Well className="entry-text">
          {noText(this.props.post.content)}
          {favoriteButton(this.props)}
        </Well>
      </div>
    );
  }
}

const favoriteButton = (props) => {
  if (!props.post.favorited) {
    return <Button className="entry-button" onClick={() => props.favoritePost(props.loginInfo, props.post.id, props.postKey)}>{props.post.favorites}<span role="img" aria-label="favorite">⭐</span></Button>
  } else {
    return <Button className="entry-button" onClick={() => props.unfavoritePost(props.loginInfo, props.post.id, props.postKey)} active>{props.post.favorites}<span role="img" aria-label="favorite">⭐</span></Button>
  }
}

function mapStateToProps({loginInfo}) {
  return {
    loginInfo
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    favoritePost,
    unfavoritePost
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PostEntry));
