import React from "react";
export const noText = (str) => {
  if(!str) {
    return(<span className="no-text">Empty field</span>);
  }
  return str;
}
