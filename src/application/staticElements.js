import React from "react";
import {withRouter} from "react-router";
import {connect} from "react-redux";

import Header from "../header/container";
import Sidebar from "../sidebar/container";

class StaticElements extends React.Component {
	render() {
		if(!this.props.loginInfo) {
			if(this.props.location.pathname !== "/") {
				alert("You can't access this page without personal account, redirecting");
				this.props.history.push("/")
			}
			
		    return(
		      <div className="empty">
		      </div>
		    );
	  	}

		return(
		    <React.Fragment>	
		      <Header />
		      <Sidebar />
		    </React.Fragment>
		);
	}
}

export default connect(({loginInfo}) => {return {loginInfo}})(withRouter(StaticElements));