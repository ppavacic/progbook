import React from "react";
import {submitPost} from "./actions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import {Form, FormGroup, FormControl, Button, Alert} from "react-bootstrap";

class NewPost extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showSuccess: false
		};
		this.showShowSuccess = this.showShowSuccess.bind(this);
		this.hideShowSuccess = this.hideShowSuccess.bind(this);
	}
	render() {
		return(
		<React.Fragment>
			<Alert style={!this.state.showSuccess ? {"display" : "none"} : {"display" : "block"}} variant="success">
				<div>Success</div>
				<p>Everyone can know what you are thinking now!</p>
			</Alert>
			<Form className="post-form" onSubmit={(event) => this.props.submitPost(event, this.props.loginInfo, this.formTextRef, this.props.linkPostExtension)}>
					<FormGroup className="post-group">
						<FormControl componentClass="textarea" placeholder="Enter your thoughts" required inputRef={ref => {this.formTextRef = ref;}}/>
						<Button type="submit" onClick={this.showShowSuccess} className="post-submit">Submit</Button>
					</FormGroup>
			</Form>
		</React.Fragment>
		);
	}

	showShowSuccess = () => {
		this.setState({
			showSuccess: true
		});
	}
	hideShowSuccess = () => {
		this.setState({
			showSuccess: false
		});
	}
}

function mapStateToProps({loginInfo, userInfo}) {
	return {
		loginInfo,
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		submitPost
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);
