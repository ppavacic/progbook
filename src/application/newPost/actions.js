import axios from "axios";
import {URL_DATABASE, SUBMIT_POST, buildDatabaseConf} from "../constants";

export const submitPost = (event, userToken, textRef, linkPostExtension) => {
  event.preventDefault();
  let value = textRef.value;
  console.log("Vrijednost toga je: " + value);
  let request = axios.post(URL_DATABASE + linkPostExtension, value, buildDatabaseConf(userToken));
  textRef.value = "";

  return {
    type: SUBMIT_POST,
    payload: request
  };
}
