import {createStore, compose, applyMiddleware} from "redux";
import ReduxPromise from "redux-promise";
import rootReducer from "./reducer";

const configureStore = () => {
  const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

  const enhancer = composeEnhancers(
    applyMiddleware(ReduxPromise),
  );
  const persistedState = loadToken();

  var store = {...createStore(rootReducer, persistedState, enhancer)};
  return store;
}

export const loadToken = () => {
  try {
    const serializedToken = localStorage.getItem("loginToken");
      if(!serializedToken) {
        return undefined;
      }
      return {
        loginInfo: JSON.parse(serializedToken)
      };
  } catch(err) {
    return undefined;
  }
}

export const saveToken = (token) => {
  try {
    const serializedToken = JSON.stringify(token);
    localStorage.setItem("loginToken", serializedToken);
  } catch {

  }
}

export const deleteState = () => {
  try {
    localStorage.setItem("loginToken", "");
  } catch {

  }
}

export default configureStore;
