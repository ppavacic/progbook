import {combineReducers} from "redux";
import {userInfo} from "../header/reducer";
import {searchBarQuery} from "../header/searchBar/reducer";
import {searchRepoList} from "../search/repoTab/reducer";
import {searchUsersList} from "../search/peopleTab/reducer";
import {userPageData} from "../user/reducer";
import {homePagePosts} from "../home/reducer";
import {followedRepoIssues, followedRepoIssuesUsers} from "../home/repoTracker/reducer";
import {userConversations} from "../conversations/reducer";
import {conversationMessages} from "../conversation/reducer";
import {myRepoList, favoritesRepoList, popularRepoList, followedRepoList} from "../sidebar/reducer";
import {loginInfo} from "../welcome/reducer";

import {LOGOUT} from "./constants";

const rootReducer = combineReducers({
  //static elements and basc info
  userInfo,
  myRepoList,
  favoritesRepoList,
  popularRepoList,

  //search page
  searchBarQuery,
  searchRepoList,
  searchUsersList,

  //user page
  userPageData,

  //home page
  homePagePosts,
  followedRepoList,
  followedRepoIssues,
  followedRepoIssuesUsers,

  //messages, conversations page
  userConversations,
  conversationMessages,

  //login info
  loginInfo,
});

const rootReducerWithLogout = (state, action) => {
  if(action.type === LOGOUT) {
    state = undefined;
  }
  return rootReducer(state, action);
}

export default rootReducerWithLogout;
