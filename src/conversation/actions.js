import axios from "axios";
import {FETCH_MESSAGES, POST_MESSAGE, URL_DATABASE, buildDatabaseConf} from "../application/constants";

export const fetchMessages = (userToken, conversationLink) => {
  let request = axios.get(URL_DATABASE + conversationLink, buildDatabaseConf(userToken));
  return {
    type: FETCH_MESSAGES,
    payload: request
  };
};


export const postMessage = (event, userToken, text, linkPostExtension) => {
  event.preventDefault();
  let request = axios.post(URL_DATABASE + linkPostExtension, text, buildDatabaseConf(userToken));

  return {
    type: POST_MESSAGE,
    payload: request
  };
}
