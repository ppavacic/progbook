import React from "react";
import {Form, FormControl, FormGroup, Button} from "react-bootstrap";
import {entryHeader} from "../application/entry/container";
import {fetchMessages, postMessage} from "./actions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {withRouter} from "react-router"
import _ from "lodash";
import "./style.css";
/*
*/
class Messages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newMessage: "",
    };
  }
  componentDidMount() {
    this.props.fetchMessages(this.props.loginInfo, this.props.location.pathname);
  }
  render() {
    if(!this.props.conversationMessages || !this.props.userInfo) return <div className="loading-animation" />;
    return(
      <div className="route messages">
        {entryHeader({
          name: this.props.conversationMessages.from_name,
          user_id: this.props.conversationMessages.from_id,
          image: this.props.conversationMessages.from_img,
          history: this.props.history
        })}
        <div className="messagesss">
          {jsonDataToMessages(this.props.conversationMessages.messages, this.props.userInfo.user_id)}
        </div>
        <Form className="post-form" id="conversation-messageInput" onSubmit={(event) => {
            this.props.postMessage(event, this.props.loginInfo, this.state.newMessage, this.props.location.pathname);
            this.setState({newMessage: ""});
          }
        }>
          <FormGroup className="post-group">
            <FormControl componentClass="textarea" placeholder="Enter your thoughts" value={this.state.newMessage} onChange={event => this.setState({newMessage: event.target.value})}/>
            <Button type="submit" className="post-submit">Submit</Button>
          </FormGroup>
         </Form>
      </div>
    );
  }
}

function mapStateToProps({conversationMessages, loginInfo, userInfo}) {
  return {
    conversationMessages,
    loginInfo,
    userInfo
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchMessages,
    postMessage
  }, dispatch);
}

const jsonDataToMessages = (data, userId) => {
  return _.map(data, (message, key) => {
    return(
        <div key={key} className="message ">
          <div className={"message-border " + (message.from_id === userId ? "s" : "r")}>
            <div className="message-data">
              {message.content_txt}
            </div>
          </div>
          <div className={"message-date " + (message.from_id === userId ? "date-s" : "date-r")}>
            {message.create_date}
          </div>
        </div>
    );
  });

}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Messages));
