import {FETCH_MESSAGES, POST_MESSAGE} from "../application/constants";

export const conversationMessages = (state = null, action) => {
  switch (action.type) {
    case FETCH_MESSAGES:
      return action.payload.data;
    case POST_MESSAGE:
      return {
        ...state,
        messages: [
          ...state.messages,
          action.payload.data
        ]
      };
    default:
      return state;
  }
};
