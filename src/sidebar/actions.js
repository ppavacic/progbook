import axios from "axios";
import {FETCH_MY_REPO_LIST, FETCH_POPULAR_REPO_LIST, FETCH_FAVORITES_REPO_LIST,
        FETCH_FOLLOWED_REPO_LIST, URL_GITLAB, URL_DATABASE, FETCH_FOLLOWED_REPO_DATA,
        GITLAB_PUB_KEY, buildGitlabConfig, buildDatabaseConf} from "../application/constants";

//GITLAB requests
export const fetchMyRepos = (userId, gitlabApiKey) => {
  let link = URL_GITLAB + "/users/" + userId  + "/projects";
  let request = axios.get(link, buildGitlabConfig(gitlabApiKey));
  return {
    type: FETCH_MY_REPO_LIST,
    payload: request
  }
}

export const fetchFollowedRepoData = (userToken, repoId, gitlabApiKey) => {
  console.log(gitlabApiKey)
  let request = axios.get(URL_GITLAB + "/projects/" + repoId, buildGitlabConfig(gitlabApiKey ? gitlabApiKey : GITLAB_PUB_KEY));
    return {
      type: FETCH_FOLLOWED_REPO_DATA,
      payload: request
    }
}

export const fetchFavoritedRepoData = (userToken, repoId) => {
  let request = axios.get(URL_GITLAB + "/projects/" + repoId, buildDatabaseConf(userToken));
    return {
      type: FETCH_FAVORITES_REPO_LIST,
      payload: request
    }
}


//DATABASESE requests
export const fetchFavoritedRepos = (userToken) => {
  let request = axios.get(URL_DATABASE + "/repo/favorites", buildDatabaseConf(userToken));
    return {
      type: FETCH_FAVORITES_REPO_LIST,
      payload: request
    }
}

export const fetchPopularRepos = () => {
  let request = axios.get(URL_DATABASE + "/repo/popular");
  return {
    type: FETCH_POPULAR_REPO_LIST,
    payload: request
  }
}

export const fetchFollowedRepos = (userToken) => {
  let request = axios.get(URL_DATABASE + "/repo/following", buildDatabaseConf(userToken));
    return {
      type: FETCH_FOLLOWED_REPO_LIST,
      payload: request
    }
}
