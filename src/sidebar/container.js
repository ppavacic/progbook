import React from "react";
import _ from "lodash";
import {bindActionCreators} from "redux";
import {connect} from "react-redux"
import {Tabs} from "react-bootstrap";
import SidebarTab from "./tab/container";
import {fetchMyRepos, fetchFavoritedRepos, fetchPopularRepos, fetchFollowedRepos, fetchFollowedRepoData} from "./actions";
import "./style.css";

//           <SidebarTab eventKey={2} repos={this.props.favoritesRepoList} title="Favorite"/>
class Sidebar extends React.Component {

  render() {
    if(!this.props.userInfo) return <div />;
    if(!this.props.followedRepoList) {
      this.props.fetchFollowedRepos(this.props.loginInfo, this.props.userInfo.gitlab_api_key);
    }

    if(!this.props.myRepoList) {
      this.props.fetchMyRepos(this.props.userInfo.gitlab_id, this.props.userInfo.gitlab_api_key);
    }

    if(this.props.followedRepoList && this.props.followedRepoList[Object.keys(this.props.followedRepoList)[0]]
                                  && !this.props.followedRepoList[Object.keys(this.props.followedRepoList)[0]].name) {
      _.map(this.props.followedRepoList, (dummy, repoId) => this.props.fetchFollowedRepoData(this.props.loginInfo, repoId, this.props.userInfo.gitlab_api_key));
    }
    return(
      <div className="static sidebar">
        <Tabs defaultActiveKey={0} id="controlled-tab-example">
          <SidebarTab eventKey={0} repos={this.props.myRepoList} title="My"/>
          <SidebarTab eventKey={1} repos={this.props.followedRepoList} title="Followed"/>
        </Tabs>
      </div>
    )
  }
}

function mapStateToProps({myRepoList, favoritesRepoList, followedRepoList, loginInfo, userInfo}) {
  return {
    userInfo,
    myRepoList,
    favoritesRepoList,
    followedRepoList,
    loginInfo
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchMyRepos,
    fetchFavoritedRepos,
    fetchPopularRepos,
    fetchFollowedRepos,
    fetchFollowedRepoData
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
