import {FETCH_MY_REPO_LIST, FETCH_POPULAR_REPO_LIST, FETCH_FAVORITES_REPO_LIST, FETCH_FOLLOWED_REPO_LIST, FETCH_FOLLOWED_REPO_DATA} from "../application/constants";

export const myRepoList = (state = null, action) => {
  switch (action.type) {
    case FETCH_MY_REPO_LIST:
      return action.payload.data;
    default:
      return state;
  }
}

export const favoritesRepoList = (state = null, action) => {
  switch (action.type) {
    case FETCH_FAVORITES_REPO_LIST:
      return action.payload.data;
    default:
      return state;
  }
}

export const popularRepoList = (state = null, action) => {
  switch (action.type) {
    case FETCH_POPULAR_REPO_LIST:
      return action.payload;
    default:
      return state;
  }
}

export const followedRepoList = (state = null, action) => {
	switch(action.type) {
		case FETCH_FOLLOWED_REPO_LIST:
			return action.payload.data.reduce((prev, next) => { prev[next] = {}; return prev; }, {});
    case FETCH_FOLLOWED_REPO_DATA:
      return {...state, [action.payload.data.id]: {
        ...action.payload.data
      }};
		default:
			return state;
	}
}
