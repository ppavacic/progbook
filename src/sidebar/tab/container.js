import React from "react";
import _ from "lodash";
import {Tab, Image} from "react-bootstrap";
import {fallBackRepoImage} from "../../application/constants";
// <Entry name={repo.name} image={repo.avatar_url} link={repo.web_url} size="small" key={key} />
const renderRepoList = (repos) => {
  if(!repos || !Object.keys(repos)[0] || !repos[Object.keys(repos)[0]].name) {
    return <div>No repositories for you in this list!</div>;
  }
  let retVal = _.map(repos, function(repo, key){
    return(
      <a href={repo.web_url} key={key}>
        <div className="entry-entry">
          <Image className="entry-image" src={repo.avatar_url ? repo.avatar_url : fallBackRepoImage} rounded responsive/>
          <div className="entry-name">
            {repo.name}
          </div>
        </div>
      </a>
    );
  })

  return retVal;
}
//{renderRepoList(this.props.repos)}
class SidebarTab extends React.Component { //this.prop.repos, this.prop.key
  render() {
    return(
      <Tab eventKey={this.props.eventKey}>
          {renderRepoList(this.props.repos)}
      </Tab>
    );
  }
}

export default SidebarTab;
