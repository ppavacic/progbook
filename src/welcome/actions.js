import axios from "axios";
import {saveToken} from "../application/store";
import {REGISTER, LOGIN, URL_DATABASE} from "../application/constants";

//REGISTER
export const register = (event) => {
  event.preventDefault();
  let formData = {
    "display_name": document.getElementById("register-name").value,
    "email": document.getElementById("register-email").value,
    "password": document.getElementById("register-password").value,
    "birth_date": document.getElementById("register-birth").value,
  };

  return axios.post(URL_DATABASE + "/register", formData)
              .then(registerSuccess)
              .catch(loginFailed);
}

const registerSuccess = (response) => {
  alert(response.data);
  return {
    type: REGISTER,
    payload: response
  }
}

//LOGIN

export const login = (event, history) => {
  event.preventDefault();
  let formData = {
    "email": document.getElementById("login-email").value,
    "password": document.getElementById("login-password").value,
  };

  return axios.post(URL_DATABASE + "/login", formData)
              .then(loginSuccess)
              .catch(loginFailed);

}

const loginFailed = (err) => {
  alert(err);
  return {
    type: LOGIN,
    payload: {
      data: null
    }
  };
}

const loginSuccess = (response) => {
  saveToken(response.data.user_token);
  return {
    type: LOGIN,
    payload: response
  };
}
