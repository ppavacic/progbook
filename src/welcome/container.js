import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Tabs, Tab} from "react-bootstrap";
import "./style.css";
import {Button, Form, FormGroup, FormControl} from "react-bootstrap";
import {login, register} from "./actions";
import 'react-datepicker/dist/react-datepicker.css';
//TODO: changed input types from e-mail to input for testing


const loginTab = (props) => {
	return(
		<Form className="login-form" onSubmit={(event) => props.login(event, props.history)}>
			<FormGroup>
				<FormControl type="input" id="login-email" placeholder="E-mail" />
				<FormControl type="password" id="login-password" placeholder="Password" />
				<Button type="submit">Log in</Button>
			</FormGroup>
		</Form>
	);
}

const registerTab = (props) => {

	return(
		<Form id="register-form" onSubmit={(event) => props.register(event)}>
			<FormGroup>
				<FormControl type="input" id="register-name" placeholder="Display name" />
				<FormControl type="email" id="register-email" placeholder="E-mail" />
				<FormControl type="password" id="register-password" placeholder="Password" />
				<FormControl type="password" placeholder="Repeat password" />
				<FormControl type="date" id="register-birth" placeholder="Date of birth"/>
				<Button type="submit">Register</Button>
			</FormGroup>
		</Form>
	);
}


class Welcome extends React.Component {
	render() {
		return(
			<div className="start route">
				<h1 className="logo clickable">Ⱂrogbook<span className="logo-cursor">|</span></h1>
				<div className="form main-form-wrapper">
					<Tabs defaultActiveKey="0" id="login-register form" className="form main-form">
						<Tab eventKey="0" title="Login">
							{loginTab(this.props)}
						</Tab>
						<Tab eventKey="1" title="Register">
							{registerTab(this.props)}
						</Tab>
					</Tabs>
				</div>
			</div>
		);
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		login,
		register
	}, dispatch);
}


export default connect(({loginInfo}) => {return {loginInfo}}, mapDispatchToProps)(Welcome);
