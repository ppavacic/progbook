import {LOGIN} from "../application/constants";
export const loginInfo = (state = null, action) => {
  switch (action.type) {
    case LOGIN:
    	return action.payload.data.user_token;
    default:
    	return state;
  }
}
