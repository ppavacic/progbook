import React from "react";
import {connect} from "react-redux";

import Home from "../home/container";
import Welcome from "../welcome/container";

class RootPage extends React.Component {
	render() {
		let rootComponent = null;
		if(this.props.loginInfo) {
			rootComponent = <Home />;
		} else {
			rootComponent = <Welcome />;
		}

		return rootComponent;
	}
}

function mapStateToProps({loginInfo}) {
	return({
		loginInfo
	});
}

export default connect(mapStateToProps)(RootPage);