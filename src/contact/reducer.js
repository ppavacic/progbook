import {FETCH_NEWCONVERSATION_DATA, DELETE_NEWCONVERSATION_DATA} from "../application/constants";

export const newConversationData = (state = null, action) => {
  switch (action.type) {
    case FETCH_NEWCONVERSATION_DATA:
      return action.payload.data;
    case DELETE_NEWCONVERSATION_DATA:
      return null;
    default:
      return state;
  }
}
