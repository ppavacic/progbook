import axios from "axios";
import {URL_DATABASE, buildDatabaseConf} from "../application/constants";

export function fetchInteractionData () {
  axios.get(URL_DATABASE + this.props.location.pathname, buildDatabaseConf(this.props.loginInfo))
    .then((response) =>
       this.setState({
         interactionData: response.data
       })
    );
}

export function postMessage (event) {
  event.preventDefault();
  axios.post(URL_DATABASE + this.props.location.pathname, this.state.newMessage, buildDatabaseConf(this.props.loginInfo))
    .then((response) => {
       this.props.history.push("/conversation/" + response.data.conv_id);
     }
    );
}
