import React from "react";
import {connect} from "react-redux";
import {Form, FormGroup, FormControl, Button} from "react-bootstrap";
import {withRouter} from "react-router";
import {entryHeader} from "../application/entry/container";
import {fetchInteractionData, postMessage} from "./actions";

class newConversation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      interactionData: null,
      newMessage: null
    };
    this.fetchInteractionData = fetchInteractionData.bind(this);
    this.postMessage = postMessage.bind(this);
    this.fetchInteractionData();
  }


  render() {
    if(!this.state.interactionData) return <div className="loading-animation" />;
    if(this.state.interactionData.exists) this.props.history.push("/conversation/" + this.state.interactionData.conv_id);
    return(
      <div className="route messages">
        {entryHeader({
          name: this.state.interactionData.interl_name,
          user_id: this.state.interactionData.interl_id,
          history: this.props.history,
          image:  this.state.interactionData.interl_img
        })}
        <Form className="post-form" onSubmit={(event) => this.postMessage(event)}>
	        <FormGroup className="post-group">
	          <FormControl componentClass="textarea" placeholder="Enter your thoughts" onChange={event => this.setState({newMessage: event.target.value})}/>
	          <Button type="submit" className="post-submit">Submit</Button>
	        </FormGroup>
    	   </Form>
      </div>
    );
  }
}

function mapStateToProps({loginInfo}) {
  return {
    loginInfo,
  };
}


export default connect(mapStateToProps)(withRouter(newConversation));
