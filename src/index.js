import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {Switch, BrowserRouter, Route} from "react-router-dom";
import configureStore from "./application/store";
import Search from "./search/container";
import User from "./user/container";
import Conversations from "./conversations/container";
import Conversation from "./conversation/container";
import RootPage from "./root/container";
import StaticElements from "./application/staticElements";
import Contact from "./contact/container.js";
import "./application/style.css";

const store = configureStore();


ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <React.Fragment>
        <Route path="/" component={StaticElements} />
        <Switch>
          <Route path="/user" component={User}/>
          <Route path="/search" component={Search}/>
          <Route path="/conversations" component={Conversations}/>
          <Route path="/contact" component={Contact}/>
          <Route path="/conversation" component={Conversation}/>
          <Route path="/" component={RootPage}/>
        </Switch>
      </React.Fragment>
    </BrowserRouter>
  </Provider>
  ,document.getElementById("root")
);
