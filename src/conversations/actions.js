import axios from "axios";
import {FETCH_CONVERSATIONS, URL_DATABASE, buildDatabaseConf} from "../application/constants";

export const fetchConversations = (userToken) => {
  let request = axios.get(URL_DATABASE + "/conversations/", buildDatabaseConf(userToken));
  return {
    type: FETCH_CONVERSATIONS,
    payload: request
  };
}
