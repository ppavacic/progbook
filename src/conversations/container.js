import React from "react";
import {withRouter} from "react-router";
import _ from "lodash";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchConversations} from "./actions";
import {Well} from "react-bootstrap";
import {entryHeader} from"../application/entry/container";

const renderPosts = (conversations, history) => {
	let convesationRendered = [];
	_.map(conversations, (data) => {
		convesationRendered.push(<div className="conversations-conversation-root">
			{entryHeader({
				name: data.interl_name,
				image: data.interl_img,
				user_id: data.interl_id,
				history: history
			})}
			<Well className="entry-text" bsSize="small" onClick={() => history.push("/conversation/" + data.conv_id)}>
					{data.conv_descr ? data.conv_descr : <div className="no-text"> {`Untitled conversation with ${data.interl_name}`} </div>}
			</Well>
		</div>);
	});

	return convesationRendered;
}

class Conversations extends React.Component {
	componentDidMount() {
		this.props.fetchConversations(this.props.loginInfo);
	}
	render() {
		if(!this.props.userConversations) {
			return (<div className="loading-animation" />)
		}

		return(
			<div className="route conversations">
				{renderPosts(this.props.userConversations, this.props.history)}
			</div>
		);
	}
}

function mapStateToProps({userConversations, loginInfo}) {
	return {
		userConversations,
		loginInfo
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		fetchConversations
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Conversations));
