import {FETCH_CONVERSATIONS} from "../application/constants";

export const userConversations = (state = null, action) => {
  switch (action.type) {
    case FETCH_CONVERSATIONS:
      return action.payload.data;
    default:
      return state;
  }
}
