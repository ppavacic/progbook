import {FETCH_HOMEPAGE_POSTS, FAVORITE_POST, UNFAVORITE_POST} from "../application/constants";

export const homePagePosts = (state = null, action) => {
	switch(action.type) {
		case FETCH_HOMEPAGE_POSTS:
			return action.payload.data;
			//#TODO apparently this creates gazilion arrays gazillion times, should normalize
		case FAVORITE_POST:
			return {...state, [action.postKey] : { // u idPostu
				...state[action.postKey], post:  {
					...state[action.postKey].post,
					favorited: true,
					favorites: state[action.postKey].post.favorites + 1
				}
			}}
			case UNFAVORITE_POST:
				return {...state, [action.postKey] : { // u idPostu
					...state[action.postKey], post:  {
						...state[action.postKey].post,
						favorited: false,
						favorites: state[action.postKey].post.favorites - 1
					}
				}}
		default:
			return state;
	}
}
