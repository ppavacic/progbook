import React from "react";
import _ from "lodash";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchHomePosts} from "./actions";
import NewPost from "../application/newPost/container";
import PostEntry from "../application/entry/post/container";
import RepoTracker from "./repoTracker/container";

class Home extends React.Component {
	componentDidMount() {
		this.props.fetchHomePosts(this.props.loginInfo);
	}
	render() {
		if(!this.props.homePagePosts) {
			return <div className="loading-animation" />
		}

		return(
		  <div className="route home">
				<RepoTracker />
				<br />
		  	<NewPost linkPostExtension="/post"/>
				{_.map(this.props.homePagePosts, function(post, key) {
					return <PostEntry usr={post.usr} post={post.post} key={key} postKey={key}/>
				})}
		  </div>
		);
	}
}

function mapStateToProps({homePagePosts, loginInfo, followedRepoIssues, followedRepoList}) {
	return{
		homePagePosts,
		loginInfo
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		fetchHomePosts,
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
