import axios from "axios";
import {FETCH_HOMEPAGE_POSTS, URL_DATABASE,
				buildDatabaseConf,} from "../application/constants";

export const fetchHomePosts = (userToken) => {
	let request = axios.get(URL_DATABASE + "/home", buildDatabaseConf(userToken));
	return {
		type: FETCH_HOMEPAGE_POSTS,
		payload: request
	}
}
