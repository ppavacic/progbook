import React from "react";
import _ from "lodash";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Carousel, Alert} from "react-bootstrap";
import {fallBackRepoImage} from "../../application/constants";
import {fetchFollowedRepoIssues, fetchFollowedRepoIssuesUsers} from "./actions";
import "./style.css";

class RepoTracker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      askedForIssues: false
    };
  }
  componentWillReceiveProps(nextProps) {
    if(this.props.followedRepoList && !this.props.followedRepoIssues && this.props.userInfo  && !this.state.askedForIssues ) {
			_.forEach(this.props.followedRepoList, (dummy, repoId) => this.props.fetchFollowedRepoIssues(repoId, this.props.userInfo.gitlab_api_key));
      this.setState({
        askedForIssues: true
      });
		}
  }
  componentWillMount() {
    if(this.props.followedRepoList && !this.props.followedRepoIssues && this.props.userInfo  && !this.state.askedForIssues ) {
      _.forEach(this.props.followedRepoList, (dummy, repoId) => this.props.fetchFollowedRepoIssues(repoId, this.props.userInfo.gitlab_api_key));
      this.setState({
        askedForIssues: true
      });
    }
  }
  render() {
    if(_.isEmpty(this.props.followedRepoList)) {
      return (
        <Alert variant="success">
  				<div>No repositories are being followed</div>
  				<p>Start following repositories and you will see important stuff on your front page!</p>
  			</Alert>
      );
    }
    if(!this.props.followedRepoList || !this.props.followedRepoIssues) {
      return <div className="loading-animation" />;
    }
    return(
      <div className="repoTracker" >
        <Carousel indicators={false} className="repoTracker">
          {!_.isEmpty(this.props.followedRepoIssues) ? _.map(this.props.followedRepoIssues, (repoIssues) => dataToDOM(repoIssues, this.props.followedRepoList)) : emptyTracker()}
        </Carousel>
      </div>
    );
  }
}

function mapStateToProps({followedRepoIssues, followedRepoList, followedRepoIssuesUsers, loginInfo, userInfo}) {
  return {
    followedRepoIssuesUsers,
    followedRepoIssues,
    followedRepoList,
    loginInfo,
    userInfo,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchFollowedRepoIssues,
    fetchFollowedRepoIssuesUsers,
  }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(RepoTracker);

function dataToDOM(repoIssues, repoList) {
  return (
    <Carousel.Item className="repoTracker-tab" interval={1000} key={repoIssues[0].project_id}>
      <div className="repoTracker-tab-title">
        <img src={repoList[repoIssues[0].project_id].avatar_url ? repoList[repoIssues[0].project_id].avatar_url : fallBackRepoImage} alt="" width="200" height="200" />
        {repoList[repoIssues[0].project_id].name}
      </div>
      {issuesToDom(repoIssues)}
    </Carousel.Item>
  );
}

const issuesToDom = (repoIssues) => {
  return _.map(repoIssues, function(issue) {
    return(
      <div key={issue.id}>
        <a href={issue.web_url}>
          <span><u>{issue.title}</u> by {issue.author.name}</span>
          <p className="repoTracker-tab-data entry-text">
            {issue.description}
          </p>
        </a>
      </div>
    );
  });
}


const emptyTracker = () => {
  return (
    <Carousel.Item className="repoTracker-tab" key={0}>
      <br />
      <img className="center" src="https://bloximages.newyork1.vip.townnews.com/smdailyjournal.com/content/tncms/assets/v3/editorial/6/53/653875f4-5df2-11e7-9519-53baa08a0925/5956e9b5b9e69.image.jpg" alt="" width="200" height="200" />

      <div className="repoTracker-tab-title">
            You already know it all
      </div>
      <p className="center">
        Important informations/news will appear here when something Important happens
      </p>
    </Carousel.Item>
  );
}
