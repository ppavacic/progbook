import axios from "axios";
import {URL_GITLAB, URL_DATABASE, buildDatabaseConf, buildGitlabConfig, FETCH_FOLLOWED_REPO_ISSUES,
        FETCH_FOLLOWED_REPO_ISSUES_USERS, GITLAB_PUB_KEY} from "../../application/constants";

export const fetchFollowedRepoIssues = (repoId, gitlabKey) => {
  let date = new Date();
  date.setDate(date.getDate() - 55);
  date = date.toISOString().slice(0, 19).replace('T', ' ');
	let request = axios.get(URL_GITLAB + `/projects/${repoId}/issues?created_after=${date}&labels=Important`,
    buildGitlabConfig(gitlabKey ? gitlabKey : GITLAB_PUB_KEY));

  return({
    type: FETCH_FOLLOWED_REPO_ISSUES,
    payload: request
  });
}

export const fetchFollowedRepoIssuesUsers = (userToken, dataList) => {
  /*console.log("Desired data: ", dataList);
  let somedata = {
    "1234": 2
  };
  console.log("SOmedata: ", somedata)*/
	let request = axios.post(URL_DATABASE + "/users/gitlabkey", null, buildDatabaseConf(userToken));

  return({
    type: FETCH_FOLLOWED_REPO_ISSUES_USERS,
    payload: request,
  });
}
