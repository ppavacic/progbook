import {FETCH_FOLLOWED_REPO_ISSUES, FETCH_FOLLOWED_REPO_ISSUES_USERS} from "../../application/constants";
export const followedRepoIssues = (state = null, action) => {
	switch (action.type) {
		case FETCH_FOLLOWED_REPO_ISSUES:
			if(action.payload.data[0]) {
				return {...state, [action.payload.data[0].project_id]: {
					...action.payload.data
					}
				};
			}
			if(!state) return {};
			else return state;
		default:
			return state;
	}
}

export const followedRepoIssuesUsers = (state = null, action) => {
	switch (action.type) {
		case FETCH_FOLLOWED_REPO_ISSUES_USERS:
			return action.data.payload;
		default:
			return state;
	}
}
