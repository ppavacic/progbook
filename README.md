**progbook** is social network with gitlab integration made using React, Redux. It's purpose is to provide a way for developers to communicate with each other and to inform developers about updates/**problems**/issues with gitlab projects that they are currently using.
This is just frontend part of the social network. You can find backend code in [progbook-backend](https://gitlab.com/ppavacic/progbook-backend) repo. 

## Features
* use of public gitlab API

* creating account
* adding gitlab API key [#TODO look for alternative way of doing this]

* private messaging
* posting text
* following people
* home page with user posts
* star posts

* searching gitlab repositories
* following gitlab repositories
* showing followed repository issues in progbook in the last *N days* marked as *Important* on gitlab as slideshow
* showing which user created those issues
* replacing gitlab_user with progbook_user on followed issues slideshow [#TODO]
* showing your gitlab account repositories 
* adding repositories to favorites

## Issues
Since this is college project which I made alone and I didnt have much time I didn't document most of the stuff, also using gitlab API key is utterly flawed. Our database sohuldn't have gitlab API key!
Extremely bad parts are tagged with #TODO
Also this software requires gitlab API key to work.